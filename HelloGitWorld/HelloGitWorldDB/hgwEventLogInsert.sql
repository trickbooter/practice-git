﻿CREATE PROCEDURE [dbo].[hgwEventLogInsert]
	@EventMessage NVARCHAR(500)
AS
/*
	Inserts an entry into EventLog
*/
BEGIN
	INSERT INTO dbo.hgwEventLog (EventMessage)
	VALUES ( @EventMessage )
	RETURN	@@ROWCOUNT
END